package paris.tedesco.paul.Launcher;

import javax.swing.JFrame;

import fr.theshark34.openlauncherlib.launcher.util.WindowMover;
import fr.theshark34.swinger.Swinger;

@SuppressWarnings("serial")
public class LauncherFrame extends JFrame{
	
	private static LauncherFrame instance;
	private LauncherPanel LauncherPanel;
	
	public LauncherFrame()
	{
		this.setTitle("Legendary World V0.1");
		this.setSize(975, 625);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setUndecorated(true);
		this.setIconImage(Swinger.getResource("icon.png"));
		
		this.setContentPane(LauncherPanel = new LauncherPanel());
		
		WindowMover mover = new WindowMover(this);
		
		this.addMouseListener(mover);
		this.addMouseMotionListener(mover);
		
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		Swinger.setSystemLookNFeel();
		Swinger.setResourcePath("/paris/tedesco/paul/ressource/");
		
		instance = new LauncherFrame();
	}
	
	public static LauncherFrame getInstance() {
		return instance;
	}

	public LauncherPanel getLauncherPanel() {
		return LauncherPanel;
	}
	

}
