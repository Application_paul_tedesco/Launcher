package paris.tedesco.paul.Launcher;

import java.io.File;
import java.io.IOException;

import fr.litarvan.openauth.AuthPoints;
import fr.litarvan.openauth.AuthenticationException;
import fr.litarvan.openauth.Authenticator;
import fr.litarvan.openauth.model.AuthAgent;
import fr.litarvan.openauth.model.response.AuthResponse;
import fr.theshark34.openlauncherlib.launcher.AuthInfos;
import fr.theshark34.openlauncherlib.launcher.GameFolder;
import fr.theshark34.openlauncherlib.launcher.GameInfos;
import fr.theshark34.openlauncherlib.launcher.GameLauncher;
import fr.theshark34.openlauncherlib.launcher.GameTweak;
import fr.theshark34.openlauncherlib.launcher.GameType;
import fr.theshark34.openlauncherlib.launcher.GameVersion;
import fr.theshark34.supdate.BarAPI;
import fr.theshark34.supdate.SUpdate;
import fr.theshark34.supdate.application.integrated.FileDeleter;
import fr.theshark34.swinger.Swinger;

public class Launcher {
	public static final GameVersion LW_VERSION = new GameVersion("1.7.10", GameType.V1_7_10);
	
	public static final GameInfos LW_INFOS = new GameInfos("Legendary World", LW_VERSION, true, new GameTweak[] {GameTweak.FORGE});
	
	public static final File LW_DIR = LW_INFOS.getGameDir();
	
	private static Thread updateThread;
	
	private static AuthInfos authInfos;
	
	public static void auth(String username, String password)throws AuthenticationException {
		Authenticator authenticator = new Authenticator(Authenticator.MOJANG_AUTH_URL, AuthPoints.NORMAL_AUTH_POINTS);
		
		AuthResponse response = authenticator.authenticate(AuthAgent.MINECRAFT, username, password, "");
		authInfos = new AuthInfos(response.getSelectedProfile().getName(), response.getAccessToken(), response.getSelectedProfile().getId());
	}
	public static void crack(String username)throws AuthenticationException {
		authInfos = new AuthInfos(username, "sry", "nope");
	}
	
	public static void update() throws Exception
	{
		SUpdate su = new SUpdate("http://legendaryworld.livehost.fr/", LW_DIR);
		su.getServerRequester().setRewriteEnabled(true);
		su.addApplication(new FileDeleter());
		updateThread = new Thread()
				{
			private int val;
			private int max;
					@Override
					public void run()
					{
						while(!this.isInterrupted()) {
							if(BarAPI.getNumberOfFileToDownload() == 0) {
								LauncherFrame.getInstance().getLauncherPanel().setInfoText("Vérification des fichier");
								continue;
								}
							val = (int) (BarAPI.getNumberOfTotalDownloadedBytes() / 10000);
							max = (int) (BarAPI.getNumberOfTotalBytesToDownload() / 10000);
							
							LauncherFrame.getInstance().getLauncherPanel().getProgressBar().setMaximum(max);
							LauncherFrame.getInstance().getLauncherPanel().getProgressBar().setValue(val);
							
							LauncherFrame.getInstance().getLauncherPanel().setInfoText("Download " + Swinger.percentage(val, max) +"% " + BarAPI.getNumberOfTotalDownloadedBytes() + "/" + BarAPI.getNumberOfTotalBytesToDownload());
						
						}
					}
				};
				updateThread.start();
				su.start();
				 if(updateThread != null) 
					 updateThread.interrupt();
				
	}
	
	public static void interruptThread()
	{
		updateThread.interrupt();
	}
	
	public static void launch() throws IOException
	{
		GameLauncher gameLauncher = new GameLauncher(LW_INFOS, GameFolder.BASIC , authInfos);
		Process p = gameLauncher.launch();;
		
		try {
			Thread.sleep(5000L);
		}catch(InterruptedException e)
		{
			
		}
		LauncherFrame.getInstance().setVisible(false);
		try {
			p.waitFor();
		}catch(InterruptedException e)
		{
			
		}
		System.exit(0);
		
		}
}
